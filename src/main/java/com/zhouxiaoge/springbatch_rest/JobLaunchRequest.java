package com.zhouxiaoge.springbatch_rest;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;

import java.util.Properties;

/**
 * @author 周小哥
 * @date 2021年08月02日 11点13分
 */
public class JobLaunchRequest {

    public String name;

    private Properties jobParameters;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Properties getJobParametersProperties() {
        return jobParameters;
    }

    public void setJobParametersProperties(Properties jobParameters) {
        this.jobParameters = jobParameters;
    }

    public JobParameters getJobParameters() {
        Properties properties = new Properties();
        properties.putAll(this.jobParameters);
        return new JobParametersBuilder(properties).toJobParameters();
    }
}
