package com.zhouxiaoge.springbatch_rest;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Properties;

/**
 * @author 周小哥
 * @date 2021年08月02日 11点06分
 */

@EnableBatchProcessing
@SpringBootApplication
public class RestApplication {

    private final JobBuilderFactory jobBuilderFactory;

    private final StepBuilderFactory stepBuilderFactory;

    public RestApplication(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
    }

    @Bean
    public Job job() {
        return this.jobBuilderFactory.get("job").incrementer(new RunIdIncrementer()).start(step()).build();
    }

    @Bean
    public Step step() {
        return this.stepBuilderFactory.get("step").tasklet((contribution, chunkContext) -> {
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~ step running ~~~~~~~~~~~~~~~~~~~~~~~~~~");
            return RepeatStatus.FINISHED;
        }).build();
    }

    public static void main(String[] args) {
        Properties properties = new Properties();
        // properties.put("spring.datasource.driver-class-name", "false");
        // properties.put("spring.batch.job.enable", "com.mysql.cj.jdbc.Driver");
        // properties.put("spring.datasource.url", "jdbc:mysql://localhost:3306/spring_batch?serverTimezone=GMT&useUnicode=true&characterEncoding=UTF-8");
        // properties.put("spring.datasource.username", "root");
        // properties.put("spring.datasource.password", "1234");
        // properties.put("spring.batch.initialize-schema", "always");
        properties.put("spring.batch.job.enabled", false);


        SpringApplication springApplication = new SpringApplication(RestApplication.class);
        springApplication.setDefaultProperties(properties);
        springApplication.run(args);
    }
}
