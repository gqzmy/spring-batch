package com.zhouxiaoge.springbatch_random_decider;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;

import java.util.Random;

/**
 * @author 周小哥
 * @date 2021年07月16日 10点21分
 */
public class RandomDecider implements JobExecutionDecider {

    private final Random random = new Random();

    @Override
    public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {
        // if (random.nextBoolean()) {
        //     return new FlowExecutionStatus(FlowExecutionStatus.COMPLETED.getName());
        // } else {
        //        return new FlowExecutionStatus(FlowExecutionStatus.FAILED.getName());
        // }
        return new FlowExecutionStatus("QQQ");
    }
}
