package com.zhouxiaoge.springbatch.step;

import com.zhouxiaoge.springbatch.item.reader.ItemReaderConfiguration;
import com.zhouxiaoge.springbatch.item.writer.ItemWriterConfiguration;
import com.zhouxiaoge.springbatch.listener.StepLoggerBeanListener;
import com.zhouxiaoge.springbatch.tasklet.CallableTasklet;
import com.zhouxiaoge.springbatch.tasklet.MethodInvokingTasklet;
import com.zhouxiaoge.springbatch.tasklet.TaskletConfiguration;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 周小哥
 * @date 2021年07月09日 15点22分
 */
@Configuration
public class StepConfiguration {

    private final ItemWriterConfiguration itemWriterConfiguration;

    private final ItemReaderConfiguration itemReaderConfiguration;

    private final MethodInvokingTasklet methodInvokingTasklet;

    private final CallableTasklet callableTasklet;

    private final StepBuilderFactory stepBuilderFactory;

    private final TaskletConfiguration taskletConfiguration;

    public StepConfiguration(StepBuilderFactory stepBuilderFactory, TaskletConfiguration taskletConfiguration, CallableTasklet callableTasklet, MethodInvokingTasklet methodInvokingTasklet, ItemWriterConfiguration itemWriterConfiguration, ItemReaderConfiguration itemReaderConfiguration) {
        this.stepBuilderFactory = stepBuilderFactory;
        this.taskletConfiguration = taskletConfiguration;
        this.callableTasklet = callableTasklet;
        this.methodInvokingTasklet = methodInvokingTasklet;
        this.itemWriterConfiguration = itemWriterConfiguration;
        this.itemReaderConfiguration = itemReaderConfiguration;
    }

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .tasklet(taskletConfiguration.step1Tasklet(null)).build();
    }

    @Bean
    public Step step2() {
        return stepBuilderFactory.get("step2")
                .tasklet(taskletConfiguration.step2Tasklet(null, null)).build();
    }

    @Bean
    public Step step3() {
        return stepBuilderFactory.get("step3")
                .tasklet(taskletConfiguration.step3Tasklet()).build();
    }

    @Bean
    public Step callableStep() {
        return stepBuilderFactory.get("callableStep")
                .tasklet(callableTasklet.taskletAdapter()).build();
    }

    @Bean
    public Step methodInvokingStep() {
        return stepBuilderFactory.get("methodInvoking")
                .tasklet(methodInvokingTasklet.methodInvoking()).build();
    }

    @Bean
    public Step methodParamsInvokingStep() {
        return stepBuilderFactory.get("methodParamsInvoking")
                .tasklet(methodInvokingTasklet.methodParamsInvoking(null)).build();
    }

    @Bean
    public Step chunkStep() {
        return stepBuilderFactory.get("chunkStep")
                .<String, String>chunk(100)
                .reader(itemReaderConfiguration.itemReader())
                .writer(itemWriterConfiguration.itemWriter())
                .listener(new StepLoggerBeanListener())
                .build();
    }
}
