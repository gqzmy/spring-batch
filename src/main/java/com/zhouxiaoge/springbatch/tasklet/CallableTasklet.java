package com.zhouxiaoge.springbatch.tasklet;

import org.springframework.batch.core.step.tasklet.CallableTaskletAdapter;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Callable;

/**
 * @author 周小哥
 * @date 2021年07月09日 18点13分
 */
@Configuration
public class CallableTasklet {

    @Bean
    public Callable<RepeatStatus> callableObject() {
        return () -> {
            System.out.println("~~~~~~~~~CallableTasklet~callableObject~~~~~~~~~");
            return RepeatStatus.FINISHED;
        };
    }

    @Bean
    public CallableTaskletAdapter taskletAdapter() {
        CallableTaskletAdapter callableTaskletAdapter = new CallableTaskletAdapter();
        callableTaskletAdapter.setCallable(callableObject());
        return callableTaskletAdapter;
    }

}
