package com.zhouxiaoge.springbatch.tasklet;

import com.zhouxiaoge.springbatch.component.CustomComponent;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.tasklet.MethodInvokingTaskletAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 周小哥
 * @date 2021年07月09日 18点24分
 */
@Configuration
public class MethodInvokingTasklet {

    @Bean
    public CustomComponent customComponent() {
        return new CustomComponent();
    }

    @Bean
    public MethodInvokingTaskletAdapter methodInvoking() {
        MethodInvokingTaskletAdapter methodInvokingTaskletAdapter = new MethodInvokingTaskletAdapter();
        methodInvokingTaskletAdapter.setTargetObject(customComponent());
        methodInvokingTaskletAdapter.setTargetMethod("componentMethod");
        return methodInvokingTaskletAdapter;
    }

    @Bean
    @StepScope
    public MethodInvokingTaskletAdapter methodParamsInvoking(@Value("#{jobParameters['message']}") String message) {
        MethodInvokingTaskletAdapter methodInvokingTaskletAdapter = new MethodInvokingTaskletAdapter();
        methodInvokingTaskletAdapter.setTargetObject(customComponent());
        methodInvokingTaskletAdapter.setTargetMethod("componentMethodParam");
        methodInvokingTaskletAdapter.setArguments(new String[]{message});
        return methodInvokingTaskletAdapter;
    }
}
