package com.zhouxiaoge.springbatch.listener;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

/**
 * @author 周小哥
 * @date 2021年07月09日 16点14分
 */
public class JobLoggerListener implements JobExecutionListener {
    @Override
    public void beforeJob(JobExecution jobExecution) {
        System.out.println("------JobLoggerListener beforeJob------" + jobExecution.getJobInstance().getJobName());
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        System.out.println("------JobLoggerListener afterJob------" + jobExecution.getJobInstance().getJobName());

    }
}
