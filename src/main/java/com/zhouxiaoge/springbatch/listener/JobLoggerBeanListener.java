package com.zhouxiaoge.springbatch.listener;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.annotation.AfterJob;
import org.springframework.batch.core.annotation.BeforeJob;

/**
 * @author 周小哥
 * @date 2021年07月09日 16点17分
 */

public class JobLoggerBeanListener {

    @BeforeJob
    public void beforeJob(JobExecution jobExecution) {
        System.out.println("------JobLoggerBeanListener beforeJob------" + jobExecution.getJobInstance().getJobName());
    }

    @AfterJob
    public void afterJob(JobExecution jobExecution) {
        System.out.println("------JobLoggerBeanListener afterJob------" + jobExecution.getJobInstance().getJobName());
    }
}
