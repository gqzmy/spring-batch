package com.zhouxiaoge.springbatch.listener;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;

/**
 * @author 周小哥
 * @date 2021年07月13日 16点56分
 */
public class StepLoggerBeanListener {

    @BeforeStep
    public void beforeStep(StepExecution stepExecution) {
        System.out.println(stepExecution.getStepName() + " beforeStep ~~~");
    }

    @AfterStep
    public void afterStep(StepExecution stepExecution) {
        System.out.println(stepExecution.getStepName() + " afterStep ~~~");
    }
}
