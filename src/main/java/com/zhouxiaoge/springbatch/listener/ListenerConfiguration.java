package com.zhouxiaoge.springbatch.listener;

import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.listener.ExecutionContextPromotionListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 周小哥
 * @date 2021年07月09日 17点06分
 */
@Configuration
public class ListenerConfiguration {

    @Bean
    public StepExecutionListener stepPromotionListener() {
        ExecutionContextPromotionListener executionContextPromotionListener = new ExecutionContextPromotionListener();
        executionContextPromotionListener.setKeys(new String[]{"name"});
        return executionContextPromotionListener;
    }
}
