package com.zhouxiaoge.springbatch.incremeter;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersIncrementer;

import java.util.Date;

/**
 * @author 周小哥
 * @date 2021年07月09日 15点15分
 */
public class DailyJobTimestamper implements JobParametersIncrementer {
    @Override
    public JobParameters getNext(JobParameters parameters) {
        return new JobParametersBuilder(parameters).addDate("currentDate", new Date()).toJobParameters();
    }
}
