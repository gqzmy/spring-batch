package com.zhouxiaoge.springbatch.component;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author 周小哥
 * @date 2021年07月09日 14点33分
 */
@Data
@Component("myJobParameters")
public class MyJobParameters {

    @Value("zhouxiaoge")
    private String name;

    @Value("文件名")
    private String fileName;
}
