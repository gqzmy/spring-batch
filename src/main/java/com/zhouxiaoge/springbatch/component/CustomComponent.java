package com.zhouxiaoge.springbatch.component;

/**
 * @author 周小哥
 * @date 2021年07月09日 18点28分
 */
public class CustomComponent {

    public void componentMethod() {
        System.out.println("~~~~~~~~CustomComponent~~~~componentMethod~~~~~~~~");
    }

    public void componentMethodParam(String params) {
        System.out.println("~~~~~~~~CustomComponent~~~~componentMethod~~~~~~~~" + params);
    }
}
