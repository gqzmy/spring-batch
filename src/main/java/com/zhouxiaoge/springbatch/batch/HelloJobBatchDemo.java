package com.zhouxiaoge.springbatch.batch;

import com.zhouxiaoge.springbatch.listener.JobLoggerListener;
import com.zhouxiaoge.springbatch.listener.ListenerConfiguration;
import com.zhouxiaoge.springbatch.step.StepConfiguration;
import com.zhouxiaoge.springbatch.validator.MyParametersValidator;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 周小哥
 * @date 2021年07月08日 15点53分
 */
@Configuration
public class HelloJobBatchDemo {

    private final MyParametersValidator myParametersValidator;

    private final ListenerConfiguration listenerConfiguration;

    private final StepConfiguration stepConfiguration;

    private final JobBuilderFactory jobBuilderFactory;

    public HelloJobBatchDemo(JobBuilderFactory jobBuilderFactory, StepConfiguration stepConfiguration, ListenerConfiguration listenerConfiguration, MyParametersValidator myParametersValidator) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepConfiguration = stepConfiguration;
        this.listenerConfiguration = listenerConfiguration;
        this.myParametersValidator = myParametersValidator;
    }

    @Bean
    public Job helloJobBatch() {
        return jobBuilderFactory.get("helloJobBatch")
                .start(stepConfiguration.step1())
                .next(stepConfiguration.step2())
                .next(stepConfiguration.callableStep())
                .next(stepConfiguration.methodInvokingStep())
                .next(stepConfiguration.methodParamsInvokingStep())
                .validator(myParametersValidator.helloJobValidator())
                .incrementer(new RunIdIncrementer())
                .listener(new JobLoggerListener())
                .listener(listenerConfiguration.stepPromotionListener())
                .build();
    }
}
