package com.zhouxiaoge.springbatch.batch;

import com.zhouxiaoge.springbatch.step.StepConfiguration;
import com.zhouxiaoge.springbatch.validator.MyParametersValidator;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 周小哥
 * @date 2021年07月13日 10点12分
 */
@Configuration
public class ChunkJobBatchDemo {

    private final StepConfiguration stepConfiguration;

    private final JobBuilderFactory jobBuilderFactory;

    public ChunkJobBatchDemo(JobBuilderFactory jobBuilderFactory, StepConfiguration stepConfiguration, MyParametersValidator myParametersValidator) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepConfiguration = stepConfiguration;
    }

    @Bean
    public Job chunkJobBatch() {
        return jobBuilderFactory.get("chunkJobBatch")
                .start(stepConfiguration.chunkStep())
                .build();
    }
}
