package com.zhouxiaoge.springbatch.batch;

import com.zhouxiaoge.springbatch.incremeter.DailyJobTimestamper;
import com.zhouxiaoge.springbatch.listener.JobLoggerBeanListener;
import com.zhouxiaoge.springbatch.step.StepConfiguration;
import com.zhouxiaoge.springbatch.validator.MyParametersValidator;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.listener.JobListenerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 周小哥
 * @date 2021年07月08日 15点53分
 */
@Configuration
public class DailyJobBatchDemo {

    private final MyParametersValidator myParametersValidator;

    private final StepConfiguration stepConfiguration;

    private final JobBuilderFactory jobBuilderFactory;

    public DailyJobBatchDemo(JobBuilderFactory jobBuilderFactory, StepConfiguration stepConfiguration, MyParametersValidator myParametersValidator) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepConfiguration = stepConfiguration;
        this.myParametersValidator = myParametersValidator;
    }

    @Bean
    public Job dailyJobBatch() {
        return jobBuilderFactory.get("dailyJobBatch")
                .start(stepConfiguration.step1())
                .next(stepConfiguration.step2())
                .validator(myParametersValidator.dailyJobValidator())
                .incrementer(new DailyJobTimestamper())
                .listener(JobListenerFactoryBean.getListener(new JobLoggerBeanListener()))
                .build();
    }

}
