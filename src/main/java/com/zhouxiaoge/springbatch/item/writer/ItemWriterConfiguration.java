package com.zhouxiaoge.springbatch.item.writer;

import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 周小哥
 * @date 2021年07月13日 10点02分
 */
@Configuration
public class ItemWriterConfiguration {

    @Bean
    public ItemWriter<String> itemWriter() {
        return items -> {
            for (String item : items) {
                System.out.println(">> current item = " + item);
            }
        };
    }
}
