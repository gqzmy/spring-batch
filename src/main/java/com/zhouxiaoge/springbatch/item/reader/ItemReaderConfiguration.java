package com.zhouxiaoge.springbatch.item.reader;

import org.springframework.batch.item.support.ListItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author 周小哥
 * @date 2021年07月13日 10点01分
 */
@Configuration
public class ItemReaderConfiguration {

    @Bean
    public ListItemReader<String> itemReader() {
        List<String> items = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            items.add(UUID.randomUUID().toString());
        }
        return new ListItemReader<>(items);
    }
}
