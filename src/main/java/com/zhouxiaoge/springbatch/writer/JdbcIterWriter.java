package com.zhouxiaoge.springbatch.writer;

import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author 周小哥
 * @date 2021年07月08日 16点56分
 */
@Component("jdbcIterWriter")
public class JdbcIterWriter implements ItemWriter<Map<String, Object>> {
    @Override
    public void write(List<? extends Map<String, Object>> items) throws Exception {
        for (Map<String, Object> item : items) {
            System.out.println(item);
        }
    }
}
