package com.zhouxiaoge.springbatch.validator;

import org.springframework.batch.core.job.CompositeJobParametersValidator;
import org.springframework.batch.core.job.DefaultJobParametersValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

/**
 * @author 周小哥
 * @date 2021年07月09日 17点15分
 */

@Configuration
public class MyParametersValidator {

    @Bean
    public CompositeJobParametersValidator dailyJobValidator() {
        CompositeJobParametersValidator validator = new CompositeJobParametersValidator();

        DefaultJobParametersValidator defaultJobParametersValidator = new DefaultJobParametersValidator(
                new String[]{"fileName"},
                new String[]{"name", "age", "currentDate", "message"}
        );

        defaultJobParametersValidator.afterPropertiesSet();
        validator.setValidators(Arrays.asList(new ParametersValidator(), defaultJobParametersValidator));

        return validator;
    }

    @Bean
    public CompositeJobParametersValidator helloJobValidator() {
        CompositeJobParametersValidator validator = new CompositeJobParametersValidator();

        DefaultJobParametersValidator defaultJobParametersValidator = new DefaultJobParametersValidator(
                new String[]{"fileName"},
                new String[]{"name", "age", "run.id", "message"}
        );

        defaultJobParametersValidator.afterPropertiesSet();
        validator.setValidators(Arrays.asList(new ParametersValidator(), defaultJobParametersValidator));

        return validator;
    }
}
